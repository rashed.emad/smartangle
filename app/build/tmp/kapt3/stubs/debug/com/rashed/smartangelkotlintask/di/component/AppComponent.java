package com.rashed.smartangelkotlintask.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001:\u0001\u0006J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&\u00a8\u0006\u0007"}, d2 = {"Lcom/rashed/smartangelkotlintask/di/component/AppComponent;", "", "inject", "", "myApplication", "Lcom/rashed/smartangelkotlintask/MyApplication;", "Builder", "app_debug"})
@dagger.Component(modules = {com.rashed.smartangelkotlintask.di.module.AppModule.class, dagger.android.AndroidInjectionModule.class, com.rashed.smartangelkotlintask.di.builder.ActivityBuilderModule.class})
@javax.inject.Singleton()
public abstract interface AppComponent {
    
    public abstract void inject(@org.jetbrains.annotations.Nullable()
    com.rashed.smartangelkotlintask.MyApplication myApplication);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u00002\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\'J\n\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/rashed/smartangelkotlintask/di/component/AppComponent$Builder;", "", "application", "Landroid/app/Application;", "build", "Lcom/rashed/smartangelkotlintask/di/component/AppComponent;", "app_debug"})
    @dagger.Component.Builder()
    public static abstract interface Builder {
        
        @org.jetbrains.annotations.Nullable()
        @dagger.BindsInstance()
        public abstract com.rashed.smartangelkotlintask.di.component.AppComponent.Builder application(@org.jetbrains.annotations.Nullable()
        android.app.Application application);
        
        @org.jetbrains.annotations.Nullable()
        public abstract com.rashed.smartangelkotlintask.di.component.AppComponent build();
    }
}