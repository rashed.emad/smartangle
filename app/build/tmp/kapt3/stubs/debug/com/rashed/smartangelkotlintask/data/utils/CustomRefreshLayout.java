package com.rashed.smartangelkotlintask.data.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\n\"\u0004\b\u000f\u0010\f\u00a8\u0006\u0014"}, d2 = {"Lcom/rashed/smartangelkotlintask/data/utils/CustomRefreshLayout;", "Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "startGestureX", "", "getStartGestureX", "()F", "setStartGestureX", "(F)V", "startGestureY", "getStartGestureY", "setStartGestureY", "onInterceptTouchEvent", "", "event", "Landroid/view/MotionEvent;", "app_debug"})
public final class CustomRefreshLayout extends androidx.swiperefreshlayout.widget.SwipeRefreshLayout {
    private float startGestureX = 0.0F;
    private float startGestureY = 0.0F;
    private java.util.HashMap _$_findViewCache;
    
    public final float getStartGestureX() {
        return 0.0F;
    }
    
    public final void setStartGestureX(float p0) {
    }
    
    public final float getStartGestureY() {
        return 0.0F;
    }
    
    public final void setStartGestureY(float p0) {
    }
    
    @java.lang.Override()
    public boolean onInterceptTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    public CustomRefreshLayout(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
}