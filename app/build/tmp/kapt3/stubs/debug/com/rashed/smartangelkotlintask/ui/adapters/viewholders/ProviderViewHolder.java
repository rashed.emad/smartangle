package com.rashed.smartangelkotlintask.ui.adapters.viewholders;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004\u00a8\u0006\u000e"}, d2 = {"Lcom/rashed/smartangelkotlintask/ui/adapters/viewholders/ProviderViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Lcom/rashed/smartangelkotlintask/databinding/ProviderViewholderBinding;", "(Lcom/rashed/smartangelkotlintask/databinding/ProviderViewholderBinding;)V", "providerViewholderBinding", "getProviderViewholderBinding", "()Lcom/rashed/smartangelkotlintask/databinding/ProviderViewholderBinding;", "setProviderViewholderBinding", "bind", "", "carProvider", "Lcom/rashed/smartangelkotlintask/data/models/CarProvider;", "Companion", "app_debug"})
public final class ProviderViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    @org.jetbrains.annotations.NotNull()
    private com.rashed.smartangelkotlintask.databinding.ProviderViewholderBinding providerViewholderBinding;
    public static final com.rashed.smartangelkotlintask.ui.adapters.viewholders.ProviderViewHolder.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.databinding.ProviderViewholderBinding getProviderViewholderBinding() {
        return null;
    }
    
    public final void setProviderViewholderBinding(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.databinding.ProviderViewholderBinding p0) {
    }
    
    public final void bind(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.models.CarProvider carProvider) {
    }
    
    public ProviderViewHolder(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.databinding.ProviderViewholderBinding view) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/rashed/smartangelkotlintask/ui/adapters/viewholders/ProviderViewHolder$Companion;", "", "()V", "create", "Lcom/rashed/smartangelkotlintask/ui/adapters/viewholders/ProviderViewHolder;", "parent", "Landroid/view/ViewGroup;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.rashed.smartangelkotlintask.ui.adapters.viewholders.ProviderViewHolder create(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}