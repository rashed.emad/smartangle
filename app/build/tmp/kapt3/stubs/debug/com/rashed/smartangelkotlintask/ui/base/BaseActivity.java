package com.rashed.smartangelkotlintask.ui.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b&\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u0002*\b\b\u0001\u0010\u0003*\u00020\u00042\u00020\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u001f\u001a\u00020 H&J\b\u0010!\u001a\u00020 H&J\u0006\u0010\"\u001a\u00020 J\b\u0010#\u001a\u00020 H&J\u0012\u0010$\u001a\u00020 2\b\u0010%\u001a\u0004\u0018\u00010&H\u0014J\b\u0010\'\u001a\u00020 H\u0002J\u0006\u0010(\u001a\u00020 J\b\u0010)\u001a\u00020 H&J\u000e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00010*H&R\u0012\u0010\u0007\u001a\u00020\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\b8gX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\nR\u001e\u0010\u0013\u001a\u0004\u0018\u00018\u0000X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001e\u0010\u0019\u001a\u0004\u0018\u00018\u0001X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001e\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001d\u00a8\u0006+"}, d2 = {"Lcom/rashed/smartangelkotlintask/ui/base/BaseActivity;", "VB", "Landroidx/databinding/ViewDataBinding;", "VM", "Lcom/rashed/smartangelkotlintask/ui/base/BaseViewModel;", "Ldagger/android/support/DaggerAppCompatActivity;", "()V", "bindingVariable", "", "getBindingVariable", "()I", "factory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "setFactory", "(Landroidx/lifecycle/ViewModelProvider$Factory;)V", "layoutId", "getLayoutId", "viewDataBinding", "getViewDataBinding", "()Landroidx/databinding/ViewDataBinding;", "setViewDataBinding", "(Landroidx/databinding/ViewDataBinding;)V", "Landroidx/databinding/ViewDataBinding;", "viewModel", "getViewModel", "()Lcom/rashed/smartangelkotlintask/ui/base/BaseViewModel;", "setViewModel", "(Lcom/rashed/smartangelkotlintask/ui/base/BaseViewModel;)V", "Lcom/rashed/smartangelkotlintask/ui/base/BaseViewModel;", "emptyIntent", "", "fetchData", "hideKeyboard", "listenToVariables", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "performDataBinding", "performDependencyInjection", "setUpView", "Ljava/lang/Class;", "app_debug"})
public abstract class BaseActivity<VB extends androidx.databinding.ViewDataBinding, VM extends com.rashed.smartangelkotlintask.ui.base.BaseViewModel> extends dagger.android.support.DaggerAppCompatActivity {
    @org.jetbrains.annotations.Nullable()
    private VB viewDataBinding;
    @org.jetbrains.annotations.Nullable()
    private VM viewModel;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public androidx.lifecycle.ViewModelProvider.Factory factory;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    public final VB getViewDataBinding() {
        return null;
    }
    
    public final void setViewDataBinding(@org.jetbrains.annotations.Nullable()
    VB p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final VM getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.Nullable()
    VM p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.ViewModelProvider.Factory getFactory() {
        return null;
    }
    
    public final void setFactory(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory p0) {
    }
    
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();
    
    /**
     * @return layout resource id
     */
    @androidx.annotation.LayoutRes()
    public abstract int getLayoutId();
    
    /**
     * Override for set view model
     *
     * @return view model instance
     */
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.Class<VM> setViewModel();
    
    /**
     * set up view and any necessary
     * binding or setting any views
     * for fetching the data.
     */
    public abstract void setUpView();
    
    /**
     * getOrderDetails any data sent to and needed
     * for fetching the data.
     */
    public abstract void emptyIntent();
    
    /**
     * fetch data
     */
    public abstract void fetchData();
    
    /**
     * listen to to variables (LiveData variables)
     * that will be updated with data,
     */
    public abstract void listenToVariables();
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void hideKeyboard() {
    }
    
    public final void performDependencyInjection() {
    }
    
    private final void performDataBinding() {
    }
    
    public BaseActivity() {
        super();
    }
}