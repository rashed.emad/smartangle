package com.rashed.smartangelkotlintask.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\b\u0010\t\u001a\u00020\nH\u0007J\b\u0010\u000b\u001a\u00020\fH\u0007\u00a8\u0006\r"}, d2 = {"Lcom/rashed/smartangelkotlintask/di/module/AppModule;", "", "()V", "getCarReposiotry", "Lcom/rashed/smartangelkotlintask/data/repository/CarRepository;", "getContext", "Landroid/content/Context;", "application", "Landroid/app/Application;", "getProvidersDataSource", "Lcom/rashed/smartangelkotlintask/data/repository/ProviderDataSource;", "getRetrofit", "Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;", "app_debug"})
@dagger.Module(includes = {com.rashed.smartangelkotlintask.di.module.ViewModelModule.class})
public final class AppModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final android.content.Context getContext(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.rashed.smartangelkotlintask.data.network.ApiWebServices getRetrofit() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.rashed.smartangelkotlintask.data.repository.CarRepository getCarReposiotry() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    public final com.rashed.smartangelkotlintask.data.repository.ProviderDataSource getProvidersDataSource() {
        return null;
    }
    
    public AppModule() {
        super();
    }
}