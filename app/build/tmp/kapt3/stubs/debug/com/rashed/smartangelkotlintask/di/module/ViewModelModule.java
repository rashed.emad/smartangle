package com.rashed.smartangelkotlintask.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\'J\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\'\u00a8\u0006\u000b"}, d2 = {"Lcom/rashed/smartangelkotlintask/di/module/ViewModelModule;", "", "()V", "bindsCarsListViewModel", "Landroidx/lifecycle/ViewModel;", "viewModel", "Lcom/rashed/smartangelkotlintask/viewmodels/CarsListViewModel;", "bindsViewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "viewModelFactory", "Lcom/rashed/smartangelkotlintask/di/module/ViewModelFactory;", "app_debug"})
@dagger.Module()
public abstract class ViewModelModule {
    
    @org.jetbrains.annotations.Nullable()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModelProvider.Factory bindsViewModelFactory(@org.jetbrains.annotations.Nullable()
    com.rashed.smartangelkotlintask.di.module.ViewModelFactory viewModelFactory);
    
    @org.jetbrains.annotations.Nullable()
    @ViewModelKey(value = com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindsCarsListViewModel(@org.jetbrains.annotations.Nullable()
    com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel viewModel);
    
    public ViewModelModule() {
        super();
    }
}