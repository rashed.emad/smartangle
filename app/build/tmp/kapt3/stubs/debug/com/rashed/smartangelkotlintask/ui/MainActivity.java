package com.rashed.smartangelkotlintask.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\b\u0010\u0010\u001a\u00020\u000eH\u0002J\b\u0010\u0011\u001a\u00020\u000eH\u0002J\b\u0010\u0012\u001a\u00020\u000eH\u0016J\b\u0010\u0013\u001a\u00020\u000eH\u0016J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u0015H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\n\u00a8\u0006\u0016"}, d2 = {"Lcom/rashed/smartangelkotlintask/ui/MainActivity;", "Lcom/rashed/smartangelkotlintask/ui/base/BaseActivity;", "Lcom/rashed/smartangelkotlintask/databinding/ActivityMainBinding;", "Lcom/rashed/smartangelkotlintask/viewmodels/CarsListViewModel;", "()V", "adapter", "Lcom/rashed/smartangelkotlintask/ui/adapters/ProviderAdapter;", "bindingVariable", "", "getBindingVariable", "()I", "layoutId", "getLayoutId", "emptyIntent", "", "fetchData", "inilizeAdapter", "listenData", "listenToVariables", "setUpView", "setViewModel", "Ljava/lang/Class;", "app_debug"})
public final class MainActivity extends com.rashed.smartangelkotlintask.ui.base.BaseActivity<com.rashed.smartangelkotlintask.databinding.ActivityMainBinding, com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel> {
    private final com.rashed.smartangelkotlintask.ui.adapters.ProviderAdapter adapter = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.Class<com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel> setViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void setUpView() {
    }
    
    @java.lang.Override()
    public void emptyIntent() {
    }
    
    @java.lang.Override()
    public void fetchData() {
    }
    
    @java.lang.Override()
    public void listenToVariables() {
    }
    
    private final void inilizeAdapter() {
    }
    
    private final void listenData() {
    }
    
    @java.lang.Override()
    public int getBindingVariable() {
        return 0;
    }
    
    @java.lang.Override()
    public int getLayoutId() {
        return 0;
    }
    
    public MainActivity() {
        super();
    }
}