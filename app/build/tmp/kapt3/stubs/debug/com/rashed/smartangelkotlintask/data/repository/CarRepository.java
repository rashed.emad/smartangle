package com.rashed.smartangelkotlintask.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u000f"}, d2 = {"Lcom/rashed/smartangelkotlintask/data/repository/CarRepository;", "", "webServices", "Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;", "provider", "Lcom/rashed/smartangelkotlintask/data/repository/ProviderDataSource;", "(Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;Lcom/rashed/smartangelkotlintask/data/repository/ProviderDataSource;)V", "getProvider", "()Lcom/rashed/smartangelkotlintask/data/repository/ProviderDataSource;", "getWebServices", "()Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;", "getProviders", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/paging/PagingData;", "Lcom/rashed/smartangelkotlintask/data/models/CarProvider;", "app_debug"})
public final class CarRepository {
    @org.jetbrains.annotations.NotNull()
    private final com.rashed.smartangelkotlintask.data.network.ApiWebServices webServices = null;
    @org.jetbrains.annotations.NotNull()
    private final com.rashed.smartangelkotlintask.data.repository.ProviderDataSource provider = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.data.network.ApiWebServices getWebServices() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.data.repository.ProviderDataSource getProvider() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.rashed.smartangelkotlintask.data.models.CarProvider>> getProviders() {
        return null;
    }
    
    @javax.inject.Inject()
    public CarRepository(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.network.ApiWebServices webServices, @org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.repository.ProviderDataSource provider) {
        super();
    }
}