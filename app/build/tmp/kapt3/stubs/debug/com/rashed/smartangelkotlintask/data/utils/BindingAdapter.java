package com.rashed.smartangelkotlintask.data.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/rashed/smartangelkotlintask/data/utils/BindingAdapter;", "", "()V", "setImageUrlRes", "", "imageView", "Landroid/widget/ImageView;", "url", "", "visibileIf", "view", "Landroid/view/View;", "showVisibile", "", "app_debug"})
public final class BindingAdapter {
    public static final com.rashed.smartangelkotlintask.data.utils.BindingAdapter INSTANCE = null;
    
    @androidx.databinding.BindingAdapter(value = {"visibileif"})
    public static final void visibileIf(@org.jetbrains.annotations.NotNull()
    android.view.View view, boolean showVisibile) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"imageUrlRes"})
    public static final void setImageUrlRes(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.Nullable()
    java.lang.String url) {
    }
    
    private BindingAdapter() {
        super();
    }
}