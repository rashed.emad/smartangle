package com.rashed.smartangelkotlintask.viewmodels;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bJ\u0006\u0010\u001f\u001a\u00020 J\u0006\u0010!\u001a\u00020 J\u0006\u0010\"\u001a\u00020 J\b\u0010#\u001a\u00020 H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000f\"\u0004\b\u0016\u0010\u0011R \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001d\u00a8\u0006$"}, d2 = {"Lcom/rashed/smartangelkotlintask/viewmodels/CarsListViewModel;", "Lcom/rashed/smartangelkotlintask/ui/base/BaseViewModel;", "carRepository", "Lcom/rashed/smartangelkotlintask/data/repository/CarRepository;", "(Lcom/rashed/smartangelkotlintask/data/repository/CarRepository;)V", "getCarRepository", "()Lcom/rashed/smartangelkotlintask/data/repository/CarRepository;", "currentSearchResult", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/paging/PagingData;", "Lcom/rashed/smartangelkotlintask/data/models/CarProvider;", "emptyResponse", "Landroidx/databinding/ObservableField;", "", "getEmptyResponse", "()Landroidx/databinding/ObservableField;", "setEmptyResponse", "(Landroidx/databinding/ObservableField;)V", "isError", "setError", "loading", "getLoading", "setLoading", "singleLiveEvent", "Lcom/rashed/smartangelkotlintask/data/other/SingleLiveEvent;", "Lcom/rashed/smartangelkotlintask/data/network/ProviderListEvent;", "getSingleLiveEvent", "()Lcom/rashed/smartangelkotlintask/data/other/SingleLiveEvent;", "setSingleLiveEvent", "(Lcom/rashed/smartangelkotlintask/data/other/SingleLiveEvent;)V", "getData", "onClicked", "", "onRefresh", "onRetryClicked", "startLoadingProviders", "app_debug"})
public final class CarsListViewModel extends com.rashed.smartangelkotlintask.ui.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private final com.rashed.smartangelkotlintask.data.repository.CarRepository carRepository = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.databinding.ObservableField<java.lang.Boolean> isError;
    @org.jetbrains.annotations.NotNull()
    private androidx.databinding.ObservableField<java.lang.Boolean> emptyResponse;
    @org.jetbrains.annotations.NotNull()
    private androidx.databinding.ObservableField<java.lang.Boolean> loading;
    @org.jetbrains.annotations.NotNull()
    private com.rashed.smartangelkotlintask.data.other.SingleLiveEvent<com.rashed.smartangelkotlintask.data.network.ProviderListEvent> singleLiveEvent;
    private kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.rashed.smartangelkotlintask.data.models.CarProvider>> currentSearchResult;
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.data.repository.CarRepository getCarRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> isError() {
        return null;
    }
    
    public final void setError(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ObservableField<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> getEmptyResponse() {
        return null;
    }
    
    public final void setEmptyResponse(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ObservableField<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> getLoading() {
        return null;
    }
    
    public final void setLoading(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ObservableField<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.data.other.SingleLiveEvent<com.rashed.smartangelkotlintask.data.network.ProviderListEvent> getSingleLiveEvent() {
        return null;
    }
    
    public final void setSingleLiveEvent(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.other.SingleLiveEvent<com.rashed.smartangelkotlintask.data.network.ProviderListEvent> p0) {
    }
    
    public final void onRefresh() {
    }
    
    public final void onClicked() {
    }
    
    public final void onRetryClicked() {
    }
    
    private final void startLoadingProviders() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<androidx.paging.PagingData<com.rashed.smartangelkotlintask.data.models.CarProvider>> getData() {
        return null;
    }
    
    @javax.inject.Inject()
    public CarsListViewModel(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.repository.CarRepository carRepository) {
        super();
    }
}