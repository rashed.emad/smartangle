package com.rashed.smartangelkotlintask.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J#\u0010\n\u001a\u0004\u0018\u00010\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\fH\u0016\u00a2\u0006\u0002\u0010\rJ+\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\u0011H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012R\u000e\u0010\u0007\u001a\u00020\u0002X\u0082D\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0013"}, d2 = {"Lcom/rashed/smartangelkotlintask/data/repository/ProviderDataSource;", "Landroidx/paging/PagingSource;", "", "Lcom/rashed/smartangelkotlintask/data/models/CarProvider;", "networkService", "Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;", "(Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;)V", "GITHUB_STARTING_PAGE_INDEX", "getNetworkService", "()Lcom/rashed/smartangelkotlintask/data/network/ApiWebServices;", "getRefreshKey", "state", "Landroidx/paging/PagingState;", "(Landroidx/paging/PagingState;)Ljava/lang/Integer;", "load", "Landroidx/paging/PagingSource$LoadResult;", "params", "Landroidx/paging/PagingSource$LoadParams;", "(Landroidx/paging/PagingSource$LoadParams;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class ProviderDataSource extends androidx.paging.PagingSource<java.lang.Integer, com.rashed.smartangelkotlintask.data.models.CarProvider> {
    private final int GITHUB_STARTING_PAGE_INDEX = 1;
    @org.jetbrains.annotations.NotNull()
    private final com.rashed.smartangelkotlintask.data.network.ApiWebServices networkService = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getRefreshKey(@org.jetbrains.annotations.NotNull()
    androidx.paging.PagingState<java.lang.Integer, com.rashed.smartangelkotlintask.data.models.CarProvider> state) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object load(@org.jetbrains.annotations.NotNull()
    androidx.paging.PagingSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super androidx.paging.PagingSource.LoadResult<java.lang.Integer, com.rashed.smartangelkotlintask.data.models.CarProvider>> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.rashed.smartangelkotlintask.data.network.ApiWebServices getNetworkService() {
        return null;
    }
    
    @javax.inject.Inject()
    public ProviderDataSource(@org.jetbrains.annotations.NotNull()
    com.rashed.smartangelkotlintask.data.network.ApiWebServices networkService) {
        super();
    }
}