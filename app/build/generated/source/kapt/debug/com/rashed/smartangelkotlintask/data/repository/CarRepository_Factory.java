// Generated by Dagger (https://dagger.dev).
package com.rashed.smartangelkotlintask.data.repository;

import com.rashed.smartangelkotlintask.data.network.ApiWebServices;
import dagger.internal.Factory;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CarRepository_Factory implements Factory<CarRepository> {
  private final Provider<ApiWebServices> webServicesProvider;

  private final Provider<ProviderDataSource> providerProvider;

  public CarRepository_Factory(Provider<ApiWebServices> webServicesProvider,
      Provider<ProviderDataSource> providerProvider) {
    this.webServicesProvider = webServicesProvider;
    this.providerProvider = providerProvider;
  }

  @Override
  public CarRepository get() {
    return newInstance(webServicesProvider.get(), providerProvider.get());
  }

  public static CarRepository_Factory create(Provider<ApiWebServices> webServicesProvider,
      Provider<ProviderDataSource> providerProvider) {
    return new CarRepository_Factory(webServicesProvider, providerProvider);
  }

  public static CarRepository newInstance(ApiWebServices webServices, ProviderDataSource provider) {
    return new CarRepository(webServices, provider);
  }
}
