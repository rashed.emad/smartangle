package com.rashed.smartangelkotlintask.databinding;
import com.rashed.smartangelkotlintask.R;
import com.rashed.smartangelkotlintask.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ProviderViewholderBindingImpl extends ProviderViewholderBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.cv_image, 4);
    }
    // views
    @NonNull
    private final com.google.android.material.card.MaterialCardView mboundView0;
    @NonNull
    private final android.widget.ImageView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ProviderViewholderBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ProviderViewholderBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.card.MaterialCardView) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[2]
            );
        this.mboundView0 = (com.google.android.material.card.MaterialCardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.ImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.textviewFullName.setTag(null);
        this.tvReName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.rashed.smartangelkotlintask.ui.ProviderItemViewModel) variable);
        }
        else if (BR.provider == variableId) {
            setProvider((com.rashed.smartangelkotlintask.data.models.CarProvider) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.rashed.smartangelkotlintask.ui.ProviderItemViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }
    public void setProvider(@Nullable com.rashed.smartangelkotlintask.data.models.CarProvider Provider) {
        this.mProvider = Provider;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.provider);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String providerImg = null;
        java.lang.String providerFullName = null;
        com.rashed.smartangelkotlintask.data.models.CarProvider provider = mProvider;
        java.lang.String providerTitleName = null;

        if ((dirtyFlags & 0x6L) != 0) {



                if (provider != null) {
                    // read provider.img
                    providerImg = provider.getImg();
                    // read provider.full_name
                    providerFullName = provider.getFull_name();
                    // read provider.title_name
                    providerTitleName = provider.getTitle_name();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.setImageUrlRes(this.mboundView1, providerImg);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textviewFullName, providerFullName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvReName, providerTitleName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): provider
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}