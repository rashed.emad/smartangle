package com.rashed.smartangelkotlintask.databinding;
import com.rashed.smartangelkotlintask.R;
import com.rashed.smartangelkotlintask.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding implements com.rashed.smartangelkotlintask.generated.callback.OnClickListener.Listener, com.rashed.smartangelkotlintask.generated.callback.OnRefreshListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.ProgressBar) bindings[3]
            , (com.google.android.material.button.MaterialButton) bindings[7]
            , (androidx.recyclerview.widget.RecyclerView) bindings[2]
            , (androidx.swiperefreshlayout.widget.SwipeRefreshLayout) bindings[0]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[5]
            );
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.progressBar.setTag(null);
        this.retryButton.setTag(null);
        this.rvProviders.setTag(null);
        this.swiperefresh.setTag(null);
        this.textView.setTag(null);
        this.textviewConnectionError.setTag(null);
        setRootTag(root);
        // listeners
        mCallback2 = new com.rashed.smartangelkotlintask.generated.callback.OnClickListener(this, 2);
        mCallback3 = new com.rashed.smartangelkotlintask.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.rashed.smartangelkotlintask.generated.callback.OnRefreshListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelLoading((androidx.databinding.ObservableField<java.lang.Boolean>) object, fieldId);
            case 1 :
                return onChangeViewModelEmptyResponse((androidx.databinding.ObservableField<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelIsError((androidx.databinding.ObservableField<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelLoading(androidx.databinding.ObservableField<java.lang.Boolean> ViewModelLoading, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEmptyResponse(androidx.databinding.ObservableField<java.lang.Boolean> ViewModelEmptyResponse, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsError(androidx.databinding.ObservableField<java.lang.Boolean> ViewModelIsError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.databinding.ObservableField<java.lang.Boolean> viewModelLoading = null;
        java.lang.Boolean viewModelEmptyResponseGet = null;
        boolean viewModelIsError = false;
        java.lang.Boolean viewModelLoadingGet = null;
        androidx.databinding.ObservableField<java.lang.Boolean> viewModelEmptyResponse = null;
        androidx.databinding.ObservableField<java.lang.Boolean> ViewModelIsError1 = null;
        java.lang.Boolean viewModelIsErrorGet = null;
        boolean viewModelLoadingViewModelIsErrorBooleanFalse = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelEmptyResponseGet = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingGet = false;
        com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel viewModel = mViewModel;
        boolean ViewModelLoading1 = false;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x1dL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.loading
                        viewModelLoading = viewModel.getLoading();
                    }
                    updateRegistration(0, viewModelLoading);


                    if (viewModelLoading != null) {
                        // read viewModel.loading.get()
                        viewModelLoadingGet = viewModelLoading.get();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loading.get())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingGet = androidx.databinding.ViewDataBinding.safeUnbox(viewModelLoadingGet);


                    // read !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loading.get())
                    ViewModelLoading1 = !androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingGet;
                if((dirtyFlags & 0x1dL) != 0) {
                    if(ViewModelLoading1) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.emptyResponse
                        viewModelEmptyResponse = viewModel.getEmptyResponse();
                    }
                    updateRegistration(1, viewModelEmptyResponse);


                    if (viewModelEmptyResponse != null) {
                        // read viewModel.emptyResponse.get()
                        viewModelEmptyResponseGet = viewModelEmptyResponse.get();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.emptyResponse.get())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelEmptyResponseGet = androidx.databinding.ViewDataBinding.safeUnbox(viewModelEmptyResponseGet);
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isError
                        ViewModelIsError1 = viewModel.isError();
                    }
                    updateRegistration(2, ViewModelIsError1);


                    if (ViewModelIsError1 != null) {
                        // read viewModel.isError.get()
                        viewModelIsErrorGet = ViewModelIsError1.get();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsErrorGet);
            }
        }
        // batch finished

        if ((dirtyFlags & 0x40L) != 0) {

                if (viewModel != null) {
                    // read viewModel.isError
                    ViewModelIsError1 = viewModel.isError();
                }
                updateRegistration(2, ViewModelIsError1);


                if (ViewModelIsError1 != null) {
                    // read viewModel.isError.get()
                    viewModelIsErrorGet = ViewModelIsError1.get();
                }


                // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get())
                androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsErrorGet);


                // read !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get())
                viewModelIsError = !androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet;
        }

        if ((dirtyFlags & 0x1dL) != 0) {

                // read !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loading.get()) ? !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get()) : false
                viewModelLoadingViewModelIsErrorBooleanFalse = ((ViewModelLoading1) ? (viewModelIsError) : (false));
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView1.setOnClickListener(mCallback2);
            this.retryButton.setOnClickListener(mCallback3);
            this.swiperefresh.setOnRefreshListener(mCallback1);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.mboundView4, androidxDatabindingViewDataBindingSafeUnboxViewModelEmptyResponseGet);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.progressBar, androidxDatabindingViewDataBindingSafeUnboxViewModelLoadingGet);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.retryButton, androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet);
            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.textView, androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet);
            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.textviewConnectionError, androidxDatabindingViewDataBindingSafeUnboxViewModelIsErrorGet);
        }
        if ((dirtyFlags & 0x1dL) != 0) {
            // api target 1

            com.rashed.smartangelkotlintask.data.utils.BindingAdapter.visibileIf(this.rvProviders, viewModelLoadingViewModelIsErrorBooleanFalse);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 2: {
                // localize variables for thread safety
                // viewModel
                com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.onClicked();
                }
                break;
            }
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.onRetryClicked();
                }
                break;
            }
        }
    }
    public final void _internalCallbackOnRefresh(int sourceId ) {
        // localize variables for thread safety
        // viewModel
        com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.onRefresh();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.loading
        flag 1 (0x2L): viewModel.emptyResponse
        flag 2 (0x3L): viewModel.isError
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loading.get()) ? !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get()) : false
        flag 6 (0x7L): !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.loading.get()) ? !androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isError.get()) : false
    flag mapping end*/
    //end
}