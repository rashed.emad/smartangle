package com.rashed.smartangelkotlintask

import android.app.Activity
import android.app.Application
import android.app.Service
import com.rashed.smartangelkotlintask.di.component.AppComponent
import com.rashed.smartangelkotlintask.di.component.DaggerAppComponent
import dagger.android.AndroidInjection.inject
import dagger.android.AndroidInjector
import dagger.android.DaggerActivity
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection.inject
import javax.inject.Inject

class MyApplication : Application() , HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
   lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>
    override fun onCreate() {
        super.onCreate()


    }
     var appComponent : AppComponent?

    init {
        appComponent = DaggerAppComponent.builder()
            .application(this)
            ?.build();
        appComponent?.inject(this);    }


    override fun androidInjector(): AndroidInjector<Any> {
        return dispatchingAndroidInjector ;
    }
}
