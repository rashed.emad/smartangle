package com.rashed.smartangelkotlintask.di.component

import android.app.Application
import com.rashed.smartangelkotlintask.MyApplication
import com.rashed.smartangelkotlintask.di.builder.ActivityBuilderModule
import com.rashed.smartangelkotlintask.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, AndroidInjectionModule::class, ActivityBuilderModule::class])
interface AppComponent {
    fun inject(myApplication: MyApplication?)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application?): Builder?
        fun build(): AppComponent?
    }
}