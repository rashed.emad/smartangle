package com.rashed.smartangelkotlintask.di.builder

import com.rashed.smartangelkotlintask.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity


}

