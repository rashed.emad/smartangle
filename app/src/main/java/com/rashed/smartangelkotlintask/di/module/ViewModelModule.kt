package com.rashed.smartangelkotlintask.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindsViewModelFactory(viewModelFactory: ViewModelFactory?): ViewModelProvider.Factory?

    @Binds
    @IntoMap
    @ViewModelKey(CarsListViewModel::class)
    abstract fun bindsCarsListViewModel(viewModel: CarsListViewModel?): ViewModel?
}