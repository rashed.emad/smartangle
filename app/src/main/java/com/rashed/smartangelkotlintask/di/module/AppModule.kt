package com.rashed.smartangelkotlintask.di.module

import android.app.Application
import android.content.Context
import com.rashed.smartangelkotlintask.data.network.ApiConstants
import com.rashed.smartangelkotlintask.data.network.ApiWebServices
import com.rashed.smartangelkotlintask.data.repository.CarRepository
import com.rashed.smartangelkotlintask.data.repository.ProviderDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module(includes = [ViewModelModule::class])
class AppModule {
    @Provides
    fun getContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    fun getRetrofit():ApiWebServices{
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(ApiConstants.serverUrl)
            .build()
        return retrofit.create(ApiWebServices::class.java)
    }
    @Provides
    fun getCarReposiotry():CarRepository{
        return CarRepository(getRetrofit(),getProvidersDataSource())
    }

    @Provides
    fun getProvidersDataSource() : ProviderDataSource {
        return ProviderDataSource(
            getRetrofit()
        )
    }


}