package com.rashed.smartangelkotlintask.ui.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.rashed.smartangelkotlintask.data.models.CarProvider
import com.rashed.smartangelkotlintask.ui.adapters.viewholders.ProviderViewHolder

class ProviderAdapter : PagingDataAdapter<CarProvider, ProviderViewHolder>(
    REPO_COMPARATOR
) {
    override fun onBindViewHolder(holder: ProviderViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            holder.bind(repoItem)
        }    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderViewHolder {
        return ProviderViewHolder.create(
            parent
        )
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<CarProvider>() {
            override fun areItemsTheSame(oldItem: CarProvider, newItem: CarProvider): Boolean =
                oldItem.title_name == newItem.title_name

            override fun areContentsTheSame(oldItem: CarProvider, newItem: CarProvider): Boolean =
                oldItem == newItem
        }
    }
}