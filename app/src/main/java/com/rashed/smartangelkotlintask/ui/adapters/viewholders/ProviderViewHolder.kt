package com.rashed.smartangelkotlintask.ui.adapters.viewholders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rashed.smartangelkotlintask.data.models.CarProvider
import com.rashed.smartangelkotlintask.databinding.ProviderViewholderBinding

class ProviderViewHolder(view: ProviderViewholderBinding): RecyclerView.ViewHolder(view.root) {
    var  providerViewholderBinding :ProviderViewholderBinding  =view
    fun bind(carProvider: CarProvider) {
providerViewholderBinding.tvReName.text =carProvider.title_name
        providerViewholderBinding.provider=carProvider

    }

    companion object {
        fun create(parent: ViewGroup): ProviderViewHolder {

            val layoutInflater = LayoutInflater.from(parent.context)
            val itemBinding: ProviderViewholderBinding =
                ProviderViewholderBinding.inflate(layoutInflater, parent, false)

            return ProviderViewHolder(
                itemBinding
            )
        }
    }

}
