package com.rashed.smartangelkotlintask.ui.base

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<VB : ViewDataBinding?, VM : BaseViewModel> :
    DaggerAppCompatActivity() {
    /*
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(LocaleHelper.onAttach(newBase)));
    }

 */
    var viewDataBinding: VB? = null
    var viewModel: VM? = null

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun setViewModel(): Class<VM>

    /**
     * set up view and any necessary
     * binding or setting any views
     * for fetching the data.
     */
    abstract fun setUpView()

    /**
     * getOrderDetails any data sent to and needed
     * for fetching the data.
     */
    abstract fun emptyIntent()

    /**
     * fetch data
     */
    abstract fun fetchData()

    /**
     * listen to to variables (LiveData variables)
     * that will be updated with data,
     */
    abstract fun listenToVariables()
    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()
        setUpView()
        emptyIntent()
        fetchData()
        listenToVariables()

    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }

    private fun performDataBinding() {
        if (layoutId != 0) viewDataBinding =
            DataBindingUtil.setContentView<VB>(this,layoutId)

        viewModel = ViewModelProvider(this, factory).get(setViewModel())
        if (viewDataBinding != null) {
            (viewDataBinding as ViewDataBinding ).setVariable(bindingVariable,viewModel)
            (viewDataBinding as ViewDataBinding )
                .setVariable(bindingVariable, viewModel)
            (viewDataBinding as ViewDataBinding ).executePendingBindings()
        }
    }


}
