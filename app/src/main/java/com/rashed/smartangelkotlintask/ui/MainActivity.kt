package com.rashed.smartangelkotlintask.ui

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rashed.smartangelkotlintask.BR
import com.rashed.smartangelkotlintask.R
import com.rashed.smartangelkotlintask.data.network.ProviderListEvent
import com.rashed.smartangelkotlintask.databinding.ActivityMainBinding
import com.rashed.smartangelkotlintask.ui.adapters.ProviderAdapter
import com.rashed.smartangelkotlintask.ui.adapters.ProviderLoadAdapter
import com.rashed.smartangelkotlintask.ui.base.BaseActivity
import com.rashed.smartangelkotlintask.viewmodels.CarsListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainActivity : BaseActivity<ActivityMainBinding, CarsListViewModel>() {
    private val adapter =
        ProviderAdapter()

    override fun setViewModel(): Class<CarsListViewModel> {
        return CarsListViewModel::class.java
    }

    override fun setUpView() {
     inilizeAdapter()
    }



    override fun emptyIntent() {
    }

    override fun fetchData() {

    }

    override fun listenToVariables() {
        viewModel?.singleLiveEvent?.observe(this, Observer {
            when (it) {

                ProviderListEvent.START->listenData()




            }
        })
        adapter.addLoadStateListener { loadState ->

            // show empty list
            viewModel?.emptyResponse?.set(loadState.refresh is LoadState.NotLoading && adapter.itemCount == 0)
            // showEmptyList(isListEmpty)

            // Only show the list if refresh succeeds.
            // viewDataBinding?.rvProviders?.isVisible = loadState.source.refresh is LoadState.NotLoading
            // Show loading spinner during initial load or refresh.
            viewModel?.loading?.set(loadState.source.refresh is LoadState.Loading)
            // Show the retry state if initial load or refresh fails.
            viewModel?.isError?.set(loadState.source.refresh is LoadState.Error)
            //   viewDataBinding?.retryButton?.isVisible = loadState.source.refresh is LoadState.Error


            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    this,
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    }
    private fun inilizeAdapter() {
        viewDataBinding?. rvProviders?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        viewDataBinding?.rvProviders?.adapter = adapter
        viewDataBinding?.rvProviders?.adapter = adapter.withLoadStateFooter(
            footer = ProviderLoadAdapter { adapter.retry() }
        )

    }

    private fun listenData() {
        viewDataBinding?.swiperefresh?.isRefreshing=false
        lifecycleScope.launch {
            viewModel?.getData()?.collectLatest { adapter.submitData(it) }
        }
    }

    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.activity_main

}