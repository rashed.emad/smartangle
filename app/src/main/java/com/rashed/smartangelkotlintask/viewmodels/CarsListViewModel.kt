package com.rashed.smartangelkotlintask.viewmodels

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rashed.smartangelkotlintask.data.models.CarProvider
import com.rashed.smartangelkotlintask.data.network.ProviderListEvent
import com.rashed.smartangelkotlintask.data.other.SingleLiveEvent
import com.rashed.smartangelkotlintask.data.repository.CarRepository
import com.rashed.smartangelkotlintask.ui.base.BaseViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CarsListViewModel @Inject constructor(carRepository: CarRepository) : BaseViewModel() {
    val carRepository = carRepository
    var isError: ObservableField<Boolean> = ObservableField(false)

    var emptyResponse: ObservableField<Boolean> = ObservableField(false)

    var loading: ObservableField<Boolean> = ObservableField(false)

    var singleLiveEvent: SingleLiveEvent<ProviderListEvent> =
        SingleLiveEvent()
    private var currentSearchResult: Flow<PagingData<CarProvider>>? = null

    init {
startLoadingProviders()
        getData()
    }


    fun onRefresh() {
startLoadingProviders()

        getData()
    }

    fun onClicked() {
        Log.d("onClicked", "onClicked: ")


    }

    fun onRetryClicked() {
        startLoadingProviders()
        getData()
    }
    private fun startLoadingProviders(){
        singleLiveEvent.value = ProviderListEvent.START

    }

    fun getData(): Flow<PagingData<CarProvider>> {

        val newResult: Flow<PagingData<CarProvider>> = carRepository.getProviders()
            .cachedIn(viewModelScope)
        currentSearchResult = newResult

        return newResult
    }
}