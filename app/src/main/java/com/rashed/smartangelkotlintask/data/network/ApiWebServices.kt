package com.rashed.smartangelkotlintask.data.network

import com.rashed.smartangelkotlintask.data.models.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiWebServices {



    @GET("provider/search?")
   suspend fun getProviders(@Query("page") page: Int, @Query("per_page") pageSize: Int): PagedResponse
}