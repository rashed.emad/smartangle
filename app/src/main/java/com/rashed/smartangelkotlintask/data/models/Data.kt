package com.rashed.smartangelkotlintask.data.models

import com.google.gson.annotations.SerializedName

data class Data(@SerializedName("current_page")val current_page:Int,@SerializedName("data") val data: List<CarProvider>) {
}