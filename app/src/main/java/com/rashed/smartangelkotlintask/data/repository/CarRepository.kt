package com.rashed.smartangelkotlintask.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.rashed.smartangelkotlintask.data.models.*
import com.rashed.smartangelkotlintask.data.network.ApiWebServices
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CarRepository @Inject constructor(webServices: ApiWebServices ,provider: ProviderDataSource) {
    val webServices = webServices
    val provider =provider



    fun getProviders(): Flow<PagingData<CarProvider>> {
            return Pager(
                config = PagingConfig(pageSize = 10, enablePlaceholders = false),
                pagingSourceFactory = { provider}
            ).flow
        }

    }

