package com.rashed.smartangelkotlintask.data.repository

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState

import com.rashed.smartangelkotlintask.data.models.CarProvider
import com.rashed.smartangelkotlintask.data.network.ApiWebServices

import javax.inject.Inject

class ProviderDataSource @Inject constructor(val networkService: ApiWebServices) :
    PagingSource<Int, CarProvider>() {

    private  val GITHUB_STARTING_PAGE_INDEX = 1


    override fun getRefreshKey(state: PagingState<Int, CarProvider>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CarProvider> {
        try {
            val position = params.key ?: 1
            // Start refresh at page 1 if undefined.
            val response = networkService.getProviders(position, 4)
            Log.d("netresomy", "load: index  $position")

            Log.d("netresomy", "load: size of  "+response.data.data.size)
            val nextKey = if (response==null ||response.data==null || response.data.data==null || response.data.data.isEmpty()) {
                null
            } else {
                // initial load size = 3 * NETWORK_PAGE_SIZE
                // ensure we're not requesting duplicating items, at the 2nd request
                position + 1
            }
            return LoadResult.Page(
                response.data.data,
                prevKey = if (position == 1) null else position - 1,
                nextKey = nextKey
            )
        } catch (e: Exception) {
            Log.d("netresomy", "load: exception " + e.message)
            Log.d("netresomy", "load: exception " + e.cause)

            return LoadResult.Error(e)
        }
    }


}

