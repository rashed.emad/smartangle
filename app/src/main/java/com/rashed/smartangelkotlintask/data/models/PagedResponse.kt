package com.rashed.smartangelkotlintask.data.models


import com.google.gson.annotations.SerializedName

data class PagedResponse(@SerializedName("status")var  status :Boolean,
                         @SerializedName("data")val data:Data
)
