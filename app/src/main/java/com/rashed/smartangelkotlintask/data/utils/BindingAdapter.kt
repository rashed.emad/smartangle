package com.rashed.smartangelkotlintask.data.utils

import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.rashed.smartangelkotlintask.R
import com.rashed.smartangelkotlintask.data.network.ApiConstants

object  BindingAdapter {


    @BindingAdapter("visibileif")
    @JvmStatic
    fun visibileIf(view: View, showVisibile: Boolean) {
        view.visibility = if (showVisibile) View.VISIBLE else View.GONE
    }

        @BindingAdapter("imageUrlRes")
        @JvmStatic
        fun setImageUrlRes(imageView: ImageView, url: String?) {


            val circularProgressDrawable =
                CircularProgressDrawable(imageView.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.setColorSchemeColors(
                ContextCompat.getColor(
                    imageView.context,
                    R.color.red
                )
            )
            circularProgressDrawable.start()
            if (TextUtils.isEmpty(url)) return
            Log.d("netresomy", "setImageUrlRes: "+ApiConstants.serverUrl+url)
            val context = imageView.context
            Glide.with(context)
                .asBitmap()
                .load(ApiConstants.serverUrl + url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(circularProgressDrawable)
                .error(
                    ContextCompat.getDrawable(
                        imageView.context,
                        R.drawable.ic_baseline_person_24
                    )
                )
                .into(imageView)
        }
    }


