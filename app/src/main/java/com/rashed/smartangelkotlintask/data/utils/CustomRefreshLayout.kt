package com.rashed.smartangelkotlintask.data.utils

import android.R
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout


class CustomRefreshLayout(context: Context, attrs: AttributeSet?) :
    SwipeRefreshLayout(context, attrs) {

    var startGestureX = 0f
    var startGestureY = 0f


    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startGestureX = event.x
                startGestureY = event.y
            }
            MotionEvent.ACTION_MOVE -> if (Math.abs(event.x - startGestureX) > Math.abs(
                    event.y - startGestureY
                )
            ) return false
        }
        return super.onInterceptTouchEvent(event)
    }
}
